var canvas = null;
var gl = null; /*instead of context we'll use gl variable*/
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;

const webGLMacros = 
{
    VML_ATTRIBUTE_POSITION : 0,
	VML_ATTRIBUTE_COLOR : 1,
	VML_ATTRIBUTE_NORMAL : 2,
	VML_ATTRIBUTE_TEXTURE0 : 3,
};

var shaderProgramObject;
var vao;
var vbo_position;
var vbo_color;

var modelMatrixUniform;
var viewMatrixUniform;
var projectionMatrixUniform;

var perspectiveProjectionMatrix;

/*below fun ptr is same like swapBuffers() in windows!*/
var requestAnimationFrame = window.requestAnimationFrame || /*google chrome*/
							window.mozRequestAnimationFrame || /*mozilla firefox*/
							window.webkitRequestAnimationFrame|| /*apple safari*/
							window.oRequestAnimationFrame || /*opera*/
							window.msRequestAnimationFrame; /*internet explorer or edge */


function main()
{
	//code
	//Step 1. Get Canvas
	canvas = document.getElementById("vml");
	if(!canvas)
	{
		console.log("Obtaining Canvas failed with given id!!\n");
	}
	else
	{
		console.log("Obtaining Canvas Success.\n");
	}

	//Backup canvas diemesions
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	//initialize
	initialize();

	//warmup resize
	resize();

	//display (no render loop in javascript)
	display();

	//Adding Keyboard & Mouse Event listeners(callbacks)
	window.addEventListener("keydown", keyDown, false);//3rd param : boolean Bubbling
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
}

function toggleFullScreen()
{
	//code
	//check browser vendor's fullscreen object is set; if not set then its not fullscreen
	var fullScreen_element = document.fullscreenElement || /*google chrome or opera*/
							 document.mozFullScreenElement || /*mozilla firefox*/
							 document.webkitFullscreenElement || /*apple safari*/
							 document.msFullscreenElement || /*Internet explorer or Edge*/
							 null;/* not fullscreen or not suppoerted by any browser*/

	if(fullScreen_element == null) /*if not fullscreen*/
	{
		if(canvas.requestFullscreen) /*google chrome or opera*/
		{
			canvas.requestFullscreen();
		}
		else if(canvas.mozRequestFullScreen) /*mozilla firefox*/
		{
			canvas.mozRequestFullScreen();	
		}
		else if(canvas.webkitRequestFullscreen) /*apple safari*/
		{
			canvas.webkitRequestFullscreen();
		}
		else if(canvas.msRequestFullscreen) /*Internet explorer or Edge*/
		{
			canvas.msRequestFullscreen();
		}

		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen) /*google chrome or opera*/
		{
			document.exitFullscreen();
		}
		else if(document.mozexitFullScreen) /*mozilla firefox*/
		{
			document.mozexitFullScreen();
		}
		else if(document.webkitexitFullScreen) /*apple safari*/
		{
			document.webkitexitFullScreen();
		}
		else if(document.msexitFullScreen) /*Internet explorer or Edge*/
		{
			document.msexitFullScreen();
		}

		bFullScreen = false;
	}
}


function initialize()
{
	//code
	//Obtain WebGL2 context
	gl = canvas.getContext("webgl2"); /*i.e internally - openGLES 3.x*/
	if(!gl)
	{
		console.log("Canvas WebGL2-Context obtaining failed!!!\n");
	}
	else
	{
		console.log("Canvas WebGL2-Context obtained.\n");
	}

	//Set ViewPort width and height of context
	gl.viewportWidth = canvas_original_width;
	gl.viewportHeight = canvas_original_height;

    //vertex shader
    var vertexShaderSourceCode = 
        "#version 300 es" +
        "\n" +
        "in vec4 a_position;" +
        "in vec4 a_color;" +
        "uniform mat4 u_modelMatrix;" +
        "uniform mat4 u_viewMatrix;" +
        "uniform mat4 u_projectionMatrix;" +
        "out vec4 a_color_out;" +
        "void main(void)" +
        "{" +
        "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" +
        "a_color_out = a_color;" +
        "}";

    var vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);

    //error checking
    if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if(error.len > 0)
        {
            alert("[Vertex Shader Compilation Log]: " + error);
            //console.log("[Vertex Shader Compilation Log]: " + error);
            uninitialize();
        }
    }

    //fragment shader
    var fragmentShaderSourceCode = 
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec4 a_color_out;" +
        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
        "FragColor = a_color_out;" +
        "}";
    
    var fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);

    //error checking
    if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(fragmentShaderObject);
        if(error.len > 0)
        {
            alert("[Fragment Shader Compilation Log]: " + error);
            //console.log("[Fragment Shader Compilation Log]: " + error);
            uninitialize();
        }
    }

    //shader program
    shaderProgramObject = gl.createProgram();
    gl.attachShader(shaderProgramObject, vertexShaderObject);
    gl.attachShader(shaderProgramObject, fragmentShaderObject);
    //pre-linking, shader arrtibute binding
    gl.bindAttribLocation(shaderProgramObject, webGLMacros.VML_ATTRIBUTE_POSITION, "a_position");
    gl.bindAttribLocation(shaderProgramObject, webGLMacros.VML_ATTRIBUTE_COLOR, "a_color");
    gl.linkProgram(shaderProgramObject);

    if(gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS) == false)
    {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        alert("[Shader Program Linking Log]:" + error);
        uninitialize();
    }

    modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_modelMatrix");
    viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_viewMatrix");
    projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projectionMatrix");

    //here start OpenGL code
    //declaration of Vertex data arrays
    var triangleVertices = new Float32Array([
        0.0, 1.0, 0.0,
		-1.0, -1.0, 0.0,
		1.0, -1.0, 0.0,
    ]);

    var triangleColor = new Float32Array([
        1.0, 0.0, 0.0,//red
      	0.0, 1.0, 0.0,//green
      	0.0, 0.0, 1.0,//blue
    ]);

    //VAO and VBO_position related code
    vao = gl.createVertexArray();
	gl.bindVertexArray(vao);
    //vbo_position
    vbo_position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position);
    gl.bufferData(gl.ARRAY_BUFFER, triangleVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(webGLMacros.VML_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(webGLMacros.VML_ATTRIBUTE_POSITION);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    //color vbo
    vbo_color = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_color);
    gl.bufferData(gl.ARRAY_BUFFER, triangleColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(webGLMacros.VML_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(webGLMacros.VML_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

	//clear the screen by black color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
    //depth related code
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	//code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

    if(canvas.height == 0)
    {
        canvas.height = 1;
    }

	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function display()
{
	//code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    //Use the shader Program Object
	gl.useProgram(shaderProgramObject);

    //local variables
    var identityMatrix = mat4.create();
    var modelMatrix = mat4.create();
    var viewMatrix = mat4.create();
    var projectionMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    //transformations
    var translationMatrix = mat4.create();
    var rotattionMatrix = mat4.create();
	    var rotattionMatrix_x = mat4.create();
	    var rotattionMatrix_y = mat4.create();
	    var rotattionMatrix_z = mat4.create();
    var scaleMatrix = mat4.create();

    //translate
    mat4.translate(translationMatrix, translationMatrix, [0.0, 0.0, -4.0]);//1st param: ouput matrix | 2nd param: input matrix | 3rd param: array of changes
    //rotate
    mat4.rotateX(rotattionMatrix_x, rotattionMatrix_x, degToRad(0.0)); //3rd parameter is angle
    mat4.rotateY(rotattionMatrix_y, rotattionMatrix_y, degToRad(0.0));
    mat4.rotateZ(rotattionMatrix_z, rotattionMatrix_z, degToRad(0.0));
    mat4.multiply(rotattionMatrix, rotattionMatrix_x, rotattionMatrix_y);
    mat4.multiply(rotattionMatrix, rotattionMatrix, rotattionMatrix_z);
    //scale
    mat4.scale(scaleMatrix, scaleMatrix, [1.0, 1.0, 1.0]);

    //multiply
	mat4.multiply(modelMatrix, translationMatrix, rotattionMatrix);
	mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);
	mat4.multiply(projectionMatrix, perspectiveProjectionMatrix, identityMatrix);

	//send uniforms
    gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
    gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
    gl.uniformMatrix4fv(projectionMatrixUniform, false, projectionMatrix);

    //draw triangle
    gl.bindVertexArray(vao);
    gl.drawArrays(gl.TRIANGLES, 0, 3);
    gl.bindVertexArray(null);

    gl.useProgram(null);

	//double buffering emulation like glSwapBuffer()
	requestAnimationFrame(display, canvas);
}

function update()
{
	//code
}

function uninitialize()
{
	//code
    //Delete/uninitialize vao
    if(vao)
    {
        gl.deleteVertexArray(vao);
        vao = null;
    }

    //Delete/uninitialize vbo_color
    if(vbo_color)
    {
        gl.deleteBuffer(vbo_color);
        vbo_color = 0;
    }

    //Delete/uninitialize vbo_position
    if(vbo_position)
    {
        gl.deleteBuffer(vbo_position);
        vbo_position = 0;
    }

    //shader uninitilazation
    if(shaderProgramObject)
    {
        gl.useProgram(shaderProgramObject);
        var shaderObjects = gl.getAttachedShaders(shaderProgramObject);
        for(let i = 0; i < shaderObjects.length; i++)
        {
            gl.detachShader(shaderProgramObject, shaderObjects[i]);
            gl.deleteShader(shaderObjects[i]);
            shaderObjects[i] = 0;
        }
        gl.useProgram(null);
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
    }
}

//keyBoard Handler
function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
	case 27: //Esc
		//event.preventdefault = false; //to disable browser's fullscreen behaviour
	case 81: //Q
	case 113://q
		uninitialize();
		if(bFullScreen == true)
		{
			toggleFullScreen();
		}
		window.close();
		break;
	case 70:
		toggleFullScreen();
		break;
	}
}

//mouse event handler
function mouseDown()
{
	//code
}

function degToRad(degrees)
{
	return degrees * Math.PI / 180.0;
}